from tkinter import *
import numpy as np
import matplotlib.pyplot as plt

def damier(): #fonction dessinant le tableau
    ligne_vert()
    ligne_hor()
        
def ligne_vert():
    c_x = 0
    while c_x != width:
        can1.create_line(c_x,0,c_x,height,width=1,fill='black')
        c_x+=c
        
def ligne_hor():
    c_y = 0
    while c_y != height:
        can1.create_line(0,c_y,width,c_y,width=1,fill='black')
        c_y+=c

def click_gauche(event): #fonction rendant vivante la cellule cliquée donc met la valeur 1 pour la cellule cliquée 
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    can1.create_rectangle(x, y, x+c, y+c, fill='black')
    mat_etat_courant[y//c,x//c]=1

def click_droit(event): #fonction tuant la cellule cliquée donc met la valeur 0 pour la cellule cliquée 
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    can1.create_rectangle(x, y, x+c, y+c, fill='orange')
    mat_etat_courant[y//c,x//c]=2
    mat_etat_suivant[y//c,x//c]=2

def change_vit(event): #fonction pour changer la vitesse(l'attente entre chaque étape)
    global vitesse
    vitesse = int(eval(entree.get()))
    print(vitesse)
    
def change_rempli(event): #fonction pour changer la vitesse(l'attente entre chaque étape)
    global rempli
    rempli = float(eval(entree2.get()))
    print(rempli)


def remplir(): #fonction remplissant le damier aléatoirement
    global mat_etat_courant,mat_etat_suivant
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            if mat_etat_courant[y,x]<2 and np.random.random()<rempli:
                mat_etat_suivant[y,x]=1
                mat_etat_courant[y,x]=1
    redessiner()

    

def go():
    "démarrage de l'animation"
    global flag
    if flag ==0:
        flag =1
        play()
        
def stop():
    "arrêt de l'animation"
    global flag,t_list,nbcell_list,t,compteur_matrice,height_mat,width_mat
    flag =0
    plt.plot(t_list,nbcell_list)
    plt.show()
    t_list.clear()
    nbcell_list.clear()
    t=0
    plt.imshow(compteur_matrice,cmap="hot")
    plt.colorbar()
    plt.show()
    compteur_matrice= np.zeros((height_mat,width_mat)) 
    
    
##############################################""    
### Calcul la distance euclidienne entre 2 points p1, p2
####################################################    
def distance(p1,p2):
    x=(p1[0]-p2[0])**2
    y=(p1[1]-p2[1])**2
    return np.sqrt(x+y)

#############################################
#####A modifier
#############################################
#############################################
### Cette fonction retourne les coordonnées x,y et la distance de la case libre voisine la plus proche.    
### En entrées : les coordonnées de la porte
###      x,y les coordonnées de le cellule courante
###      mat_etat_suivant : l'état suivant de la matrice    
def calculcaseprocheetlibre(porte,x,y,mat_etat_suivant):
    min_distance=999999
    min_coordonnee=(-1,-1)
    if mat_etat_suivant[y-1,x] ==0:
        d=distance(porte,(y-1,x))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y-1,x)
    
    if mat_etat_suivant[y,x-1] ==0:
        d=distance(porte,(y,x-1))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y,x-1)
    
    if mat_etat_suivant[y-1,x-1] ==0:
        d=distance(porte,(y-1,x-1))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y-1,x-1)

    if mat_etat_suivant[y+1,x] ==0:
        d=distance(porte,(y+1,x))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y+1,x)
    
            
    if mat_etat_suivant[y,x+1] ==0:
        d=distance(porte,(y,x+1))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y,x+1)
    
    if mat_etat_suivant[y+1,x+1] ==0:
        d=distance(porte,(y+1,x+1))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y+1,x+1)
    
    if mat_etat_suivant[y-1,x+1] ==0:
        d=distance(porte,(y-1,x+1))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y-1,x+1)
    
    
    if mat_etat_suivant[y+1,x-1] ==0:
        d=distance(porte,(y+1,x-1))
        if(min_distance>d):
            min_distance=d
            min_coordonnee=(y+1,x-1)
    
    return min_distance,min_coordonnee
    
    
def play(): #fonction comptant le nombre de cellules vivantes autour de chaque cellule
    global flag, vitesse,mat_etat_courant,mat_etat_suivant,t,t_list,nbcell_list,porte,compteur_matrice

    

    nbcellvivante=0
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            
            if mat_etat_courant[y,x]==1:
                min_distance,min_coordonnee=calculcaseprocheetlibre(porte,x,y,mat_etat_suivant)
                if min_coordonnee[0]==porte[0] and min_coordonnee[1]==porte[1]:
                    mat_etat_suivant[y,x]=0
                elif min_coordonnee[0]==-1 and min_coordonnee[1]==-1:
                    mat_etat_suivant[y,x]=1
                    compteur_matrice[y,x]+=1
                else:    
                    mat_etat_suivant[min_coordonnee[0],min_coordonnee[1]]=1
                    mat_etat_suivant[y,x]=0
                    compteur_matrice[min_coordonnee[0],min_coordonnee[1]]+=1
            
                

    t_list.append(t)
    nbcell_list.append(mat_etat_suivant.sum())
    t=t+1
    redessiner()
    mat_etat_courant=mat_etat_suivant.copy()
    if flag >0: 
        fen1.after(vitesse,play)

        

def redessiner(): #fonction redessinant le tableau à partir de mat_etat
    can1.delete(ALL)
    damier()
    
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            w=x*c
            h=y*c
            if mat_etat_suivant[y,x]==1:
                can1.create_rectangle(w, h, w+c, h+c, fill='black')
            elif mat_etat_suivant[y,x]==2:
                can1.create_rectangle(w, h, w+c, h+c, fill='orange')
            else:
                can1.create_rectangle(w, h, w+c, h+c, fill='white')
    
    w=c*porte[1]
    h=c*porte[0]
    can1.create_rectangle(w, h, w+c, h+c, fill='blue')
    
#les différentes variables:

# taille de la grille
height = 400
width = 400

#taille des cellules
c = 10

#taille des matrices
width_mat=width//c
height_mat=height//c


#vitesse de l'animation (en réalité c'est l'attente entre chaque étapes en ms)
vitesse=50

#taux de remplissage
rempli=0.5

#le nombre d'itération
t=0

t_list=[]
nbcell_list=[]

flag=0
mat_etat_courant = np.zeros((height_mat,width_mat)) #matrice contenant le nombre de cellules vivantes autour de chaque cellule
mat_etat_suivant = np.zeros((height_mat,width_mat))  #matrice pour l'état suivant
compteur_matrice= np.zeros((height_mat,width_mat)) 
porte=(20,1)
#programme "principal" 
fen1 = Tk()

can1 = Canvas(fen1, width =width, height =height, bg ='white')
can1.bind("<Button-1>", click_gauche)
can1.bind("<Button-3>", click_droit)
can1.pack(side =TOP, padx =5, pady =5)

damier()

#On dessine la porte
w=c*porte[1]
h=c*porte[0]
can1.create_rectangle(w, h, w+c, h+c, fill='blue')

b1 = Button(fen1, text ='Go!', command =go)
b2 = Button(fen1, text ='Stop', command =stop)
b1.pack(side =LEFT, padx =3, pady =3)
b2.pack(side =LEFT, padx =3, pady =3)

b3 = Button(fen1, text ='Remplir', command =remplir)
b3.pack(side =LEFT, padx =3, pady =3)

entree = Entry(fen1)
entree.bind("<Return>", change_vit)
entree.pack(side =RIGHT)
chaine = Label(fen1)
chaine.configure(text = "Attente entre chaque étape (ms) :")
chaine.pack(side =RIGHT)

entree2 = Entry(fen1)
entree2.bind("<Return>", change_rempli)
entree2.pack(side =RIGHT)
chaine2 = Label(fen1)
chaine2.configure(text = "Taux remplissage :")
chaine2.pack(side =RIGHT)


fen1.mainloop()
