from tkinter import *
import numpy as np

def damier(): #fonction dessinant le tableau
    ligne_vert()
    ligne_hor()
        
def ligne_vert():
    c_x = 0
    while c_x != width:
        can1.create_line(c_x,0,c_x,height,width=1,fill='black')
        c_x+=c
        
def ligne_hor():
    c_y = 0
    while c_y != height:
        can1.create_line(0,c_y,width,c_y,width=1,fill='black')
        c_y+=c

def click_gauche(event): #fonction rendant vivante la cellule cliquée donc met la valeur 1 pour la cellule cliquée 
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    can1.create_rectangle(x, y, x+c, y+c, fill='black')
    mat_etat_courant[y//c,x//c]=1

def click_droit(event): #fonction tuant la cellule cliquée donc met la valeur 0 pour la cellule cliquée
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    can1.create_rectangle(x, y, x+c, y+c, fill='white')
    mat_etat_courant[y//c,x//c]=0

def change_vit(event): #fonction pour changer la vitesse(l'attente entre chaque étape)
    global vitesse
    vitesse = int(eval(entree.get()))
    print(vitesse)
    
    

def go():
    "démarrage de l'animation"
    global flag
    if flag ==0:
        flag =1
        play()
        
def stop():
    "arrêt de l'animation"
    global flag
    flag =0
    
##############
# à compléter par les étudiants
##############    
def play(): #fonction comptant le nombre de cellules vivantes autour de chaque cellule
    global flag, vitesse,mat_etat_courant,mat_etat_suivant
    
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            #appliquer les régles du jeu de la vie
            #ici ##########
            print("debug=",x,y)

    redessiner()
    mat_etat_courant=mat_etat_suivant.copy()
    if flag >0: 
        fen1.after(vitesse,play)

        

def redessiner(): #fonction redessinant le tableau à partir de mat_etat
    can1.delete(ALL)
    damier()
    
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            w=x*c
            h=y*c
            if mat_etat_suivant[y,x]==1:
                can1.create_rectangle(w, h, w+c, h+c, fill='black')
            else:
                can1.create_rectangle(w, h, w+c, h+c, fill='white')
        
    
#les différentes variables:

# taille de la grille
height = 400
width = 400

#taille des cellules
c = 10

#la taille des matrices sans tenir compte de la taille des cellules
width_mat=width//c
height_mat=height//c


#vitesse de l'animation (en réalité c'est l'attente entre chaque étapes en ms)
vitesse=50


flag=0
mat_etat_courant = np.zeros((height_mat,width_mat)) #matrice contenant le nombre de cellules vivantes autour de chaque cellule
mat_etat_suivant = np.zeros((height_mat,width_mat))  #matrice pour l'état suivant

##Ce code n'est pas utile mais sert à illustrer le parcours des matrices
for x in range(0,width_mat):
    for y in range(0,height_mat):
        mat_etat_courant[y,x]=0  #assigne une valeur 0(morte) a chaque coordonnées(cellules) (valeur par défault en quelque sorte ^^)
        mat_etat_suivant[y,x]=0

#programme "principal" 
fen1 = Tk()

can1 = Canvas(fen1, width =width, height =height, bg ='white')
can1.bind("<Button-1>", click_gauche)
can1.bind("<Button-3>", click_droit)
can1.pack(side =TOP, padx =5, pady =5)

damier()

b1 = Button(fen1, text ='Go!', command =go)
b2 = Button(fen1, text ='Stop', command =stop)
b1.pack(side =LEFT, padx =3, pady =3)
b2.pack(side =LEFT, padx =3, pady =3)


entree = Entry(fen1)
entree.bind("<Return>", change_vit)
entree.pack(side =RIGHT)
chaine = Label(fen1)
chaine.configure(text = "Attente entre chaque étape (ms) :")
chaine.pack(side =RIGHT)


fen1.mainloop()
