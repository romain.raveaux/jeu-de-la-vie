from tkinter import *
import numpy as np
import matplotlib.pyplot as plt

def damier(): #fonction dessinant le tableau
    ligne_vert()
    ligne_hor()
        
def ligne_vert():
    c_x = 0
    while c_x != width:
        can1.create_line(c_x,0,c_x,height,width=1,fill='black')
        c_x+=c
        
def ligne_hor():
    c_y = 0
    while c_y != height:
        can1.create_line(0,c_y,width,c_y,width=1,fill='black')
        c_y+=c

def click_gauche(event): #fonction rendant vivante la cellule cliquée donc met la valeur 1 pour la cellule cliquée 
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    can1.create_rectangle(x, y, x+c, y+c, fill='black')
    mat_etat_courant[y//c,x//c]=1

def click_droit(event): #fonction tuant la cellule cliquée donc met la valeur 0 pour la cellule cliquée 
    x = event.x -(event.x%c)
    y = event.y -(event.y%c)
    can1.create_rectangle(x, y, x+c, y+c, fill='white')
    mat_etat_courant[y//c,x//c]=0

def change_vit(event): #fonction pour changer la vitesse(l'attente entre chaque étape)
    global vitesse
    vitesse = int(eval(entree.get()))
    print(vitesse)
    
def change_rempli(event): #fonction pour changer la vitesse(l'attente entre chaque étape)
    global rempli
    rempli = float(eval(entree2.get()))
    print(rempli)


def remplir(): #fonction remplissant le damier aléatoirement
    global mat_etat_courant,mat_etat_suivant
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            if np.random.random()<rempli:
                mat_etat_suivant[y,x]=1
                mat_etat_courant[y,x]=1
    redessiner()

    

def go():
    "démarrage de l'animation"
    global flag
    if flag ==0:
        flag =1
        play()
        
def stop():
    "arrêt de l'animation"
    global flag,t_list,nbcell_list,t
    flag =0
    plt.plot(t_list,nbcell_list)
    plt.show()
    t_list.clear()
    nbcell_list.clear()
    t=0
    
    
def play(): #fonction comptant le nombre de cellules vivantes autour de chaque cellule
    global flag, vitesse,mat_etat_courant,mat_etat_suivant,t,t_list,nbcell_list

    

    nbcellvivante=0
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            
            compt_viv=0
            compt_viv+=mat_etat_courant[y-1,x]
            compt_viv+=mat_etat_courant[y,x-1]
            compt_viv+=mat_etat_courant[y-1,x-1]
            compt_viv+=mat_etat_courant[y+1,x]
            compt_viv+=mat_etat_courant[y,x+1]
            compt_viv+=mat_etat_courant[y+1,x+1]
            compt_viv+=mat_etat_courant[y-1,x+1]
            compt_viv+=mat_etat_courant[y+1,x-1]
            
            nbcellvivante+=compt_viv
            

              #regle 1
            if compt_viv==3:
                mat_etat_suivant[y,x]=1            
            #regle 2
            if compt_viv==2:
                mat_etat_suivant[y,x]=mat_etat_courant[y,x]
            #regle 3
            if compt_viv<2 or compt_viv>3:
                mat_etat_suivant[y,x]=0

    t_list.append(t)
    nbcell_list.append(nbcellvivante)
    t=t+1
    redessiner()
    mat_etat_courant=mat_etat_suivant.copy()
    if flag >0: 
        fen1.after(vitesse,play)

        

def redessiner(): #fonction redessinant le tableau à partir de mat_etat
    can1.delete(ALL)
    damier()
    
    for x in range(1,width_mat-1):
        for y in range(1,height_mat-1):
            w=x*c
            h=y*c
            if mat_etat_suivant[y,x]==1:
                can1.create_rectangle(w, h, w+c, h+c, fill='black')
            else:
                can1.create_rectangle(w, h, w+c, h+c, fill='white')
        
    
#les différentes variables:

# taille de la grille
height = 400
width = 400

#taille des cellules
c = 10

#taille des matrices
width_mat=width//c
height_mat=height//c


#vitesse de l'animation (en réalité c'est l'attente entre chaque étapes en ms)
vitesse=50

#taux de remplissage
rempli=0.5

#le nombre d'itération
t=0

t_list=[]
nbcell_list=[]

flag=0
mat_etat_courant = np.zeros((height_mat,width_mat)) #matrice contenant le nombre de cellules vivantes autour de chaque cellule
mat_etat_suivant = np.zeros((height_mat,width_mat))  #matrice pour l'état suivant

#programme "principal" 
fen1 = Tk()

can1 = Canvas(fen1, width =width, height =height, bg ='white')
can1.bind("<Button-1>", click_gauche)
can1.bind("<Button-3>", click_droit)
can1.pack(side =TOP, padx =5, pady =5)

damier()

b1 = Button(fen1, text ='Go!', command =go)
b2 = Button(fen1, text ='Stop', command =stop)
b1.pack(side =LEFT, padx =3, pady =3)
b2.pack(side =LEFT, padx =3, pady =3)

b3 = Button(fen1, text ='Remplir', command =remplir)
b3.pack(side =LEFT, padx =3, pady =3)

entree = Entry(fen1)
entree.bind("<Return>", change_vit)
entree.pack(side =RIGHT)
chaine = Label(fen1)
chaine.configure(text = "Attente entre chaque étape (ms) :")
chaine.pack(side =RIGHT)

entree2 = Entry(fen1)
entree2.bind("<Return>", change_rempli)
entree2.pack(side =RIGHT)
chaine2 = Label(fen1)
chaine2.configure(text = "Taux remplissage :")
chaine2.pack(side =RIGHT)


fen1.mainloop()
